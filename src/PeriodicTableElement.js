function PeriodicTableElement(props) {
    var description = props.description
    var color = '#ffffff'
    if (description['cpk-hex']) {
        color = '#' + description['cpk-hex']
    }
    let style = {
        gridColumn: description.xpos,
        gridRow: description.ypos,
    }
    console.log(style)
    return (
        <div className='Element' style={style}>
            <div className='ElementUpperContainer'>
                <div className='ElementUpperProperty ElementProperty'>{description.number}</div>
                <div className='ElementUpperProperty ElementProperty'>{description.atomic_mass.toPrecision(3)}</div>
            </div>
            <div style={{ backgroundColor: color }} className='ElementColor ElementProperty'></div>
            <div className='ElementSymbol ElementProperty'>{description.symbol}</div>
            <div className='ElementName ElementProperty'>{description.name}</div>
        </div>
    );
}

export default PeriodicTableElement;
