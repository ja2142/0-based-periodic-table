import PeriodicTableElement from './PeriodicTableElement';
import React from 'react';
import elements from './PeriodicTableJSON.json';

class PeriodicTable extends React.Component {
    createElement(element_desc) {
        return <PeriodicTableElement
            key={element_desc.symbol}
            description={element_desc}/>
    }

    createElements(elements_descs) {
        return elements_descs.map(this.createElement)
    }

    fixFBlockPositions(elements_descs) {
        return elements_descs.map((element) => {
            if (element.ypos >= 9) {
                // move f-block metals to their proper place
                return {...element, ypos: element.ypos-3}
            } else if(element.xpos >= 3) {
                // move everything but f and s block metals to fit f-block
                return {...element, xpos: element.xpos+14}
            } else {
                return element
            }
        })
    }

    fixNoblePositions(elements_descs) {
        const nobles = [2, 10, 18, 36, 54, 86, 118]
        return elements_descs.map((element) => {
            if (nobles.includes(element.number)) {
                // move nobles to 0th column (well, first, not my fault that grid-column starts from 1)
                return {...element, xpos: 1, ypos: element.ypos + 1}
            } else {
                // move everything but nobles to fit nobles
                return {...element, xpos: element.xpos + 1}
            }
        })
    }

    prepareData() {
        var elements_fixed = this.fixFBlockPositions(elements.elements)
        elements_fixed = this.fixNoblePositions(elements_fixed)
        elements_fixed.push({
            "name": "Nothing",
            "atomic_mass": 0,
            "named_by": "me",
            "number": 0,
            "symbol": "Nl",
            "xpos": 1,
            "ypos": 1,
            "cpk-hex": "000000"
        })
        return elements_fixed
    }

    render() {
        return (
            <div className='PeriodicTable'>
                {this.createElements(this.prepareData())}
            </div>
        );
    }
};

export default PeriodicTable;
