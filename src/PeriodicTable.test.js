import { render, screen } from '@testing-library/react';
import PeriodicTable from './PeriodicTable';

test('renders nothing element', () => {
    render(<PeriodicTable />);
    const nothingElement = screen.getByText(/Nothing/i);
    expect(nothingElement).toBeInTheDocument();
});
