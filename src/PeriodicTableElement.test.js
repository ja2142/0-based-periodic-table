import { render, screen } from '@testing-library/react';
import PeriodicTableElement from './PeriodicTableElement';

const element_desc = {
    "name": "Test",
    "atomic_mass": 10.00003,
    "named_by": "me",
    "number": 123,
    "symbol": "Te",
    "xpos": 1,
    "ypos": 1,
    "cpk-hex": "1289ef"
}

test('renders element name', () => {
    render(<PeriodicTableElement description={element_desc} />);
    const nothingElement = screen.getByText(/Test/i);
    expect(nothingElement).toBeInTheDocument();
});

test('rounds atomic mass', () => {
    render(<PeriodicTableElement description={element_desc} />);
    const atomicMass = screen.getByText(/10\.0/i);
    expect(atomicMass).toBeInTheDocument();
});
