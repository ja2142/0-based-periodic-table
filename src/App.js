import './App.css';
import PeriodicTable from './PeriodicTable.js'

function App() {
    return (
        <div>
            <PeriodicTable />
        </div>
    );
}

export default App;
