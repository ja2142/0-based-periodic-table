# 0-based periodic table

https://ja2142.gitlab.io/0-based-periodic-table/

In my opinion Hydrogen and Helium make periodic table a little bit to jagged and counting should start from 0 in most cases (looking at you lua...).

This is a periodic table with group 18 changed to group 0, which fixes both of these problems.

Data taken from https://github.com/Bowserinator/Periodic-Table-JSON.

I didn't know much about alternative periodic table arangements, but as it turn out, there are great many alternatives
(https://en.wikipedia.org/wiki/Alternative_periodic_tables - left step and anything spiral ftw).

Run with `npm start`.
